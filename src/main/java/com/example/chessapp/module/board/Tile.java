package com.example.chessapp.module.board;

import com.example.chessapp.module.pieces.Piece;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;


public abstract class Tile {

    // fields
    protected final int tileCoordinate;
    private static final Map<Integer, Empty> EMPTY_TILE_MAP = createEmptyTiles();


    // constructors
    private Tile(final int tileCoordinate) {
        this.tileCoordinate = tileCoordinate;
    }

    // methods

    // creating empty tiles, and making them immutable.
    private static Map<Integer, Empty> createEmptyTiles() {
        // initialize empty map for tiles
        final Map<Integer, Empty> emptyTileMap = new HashMap<>();

        // for loop to create 64 tiles
        for (int i = 0; i < BoardUtils.NUMBER_OF_TILES; i++) {
            emptyTileMap.put(i, new Empty(i));
        }
        return ImmutableMap.copyOf(emptyTileMap);
    }

    // create an occupied tile or return tile coordinate if the piece is null
    public static Tile createTile(final int tileCoordinate, final Piece piece) {
        return piece != null ? new Occupied(tileCoordinate, piece) : EMPTY_TILE_MAP.get(tileCoordinate);
    }

    // abstract method to check the state of a given tile
    public abstract boolean isOccupied();

    // abstract method to get information about the piece on the given tile
    // returns null if the tile is not occupied
    public abstract Piece getPiece();


    /* -------------------------------------------------------------------------------------
    Inner Static class for empty tiles
    *  It is static, because we don't want it to inherit
    * the state of the super class
    * -------------------------------------------------------------------------------------- */
    public static final class Empty extends Tile {

        // constructors
        private Empty(final int tileCoordinate) {
            super(tileCoordinate);
        }

        // method to check the state of a given tile
        @Override
        public boolean isOccupied() {
            return false;
        }

        // a method to get information about the piece on the given tile
        // returns null if the tile is not occupied
        @Override
        public Piece getPiece() {
            return null;
        }
    }

    /* ------------------------------------------------------------------------------------------------
    Inner Static class for occupied tiles
     *  It is static, because we don't want it to inherit
     * the state of the super class
     * ------------------------------------------------------------------------------------------------ */
    public static final class Occupied extends Tile {

        //fields
        private final Piece pieceOnSpecificTile;

        // constructors
        private Occupied(final int tileCoordinate, final Piece pieceOnSpecificTile) {
            super(tileCoordinate);
            this.pieceOnSpecificTile = pieceOnSpecificTile;
        }

        // a method to check the state of a given tile
        @Override
        public boolean isOccupied() {
            return false;
        }

        // a method to get information about the piece on the given tile
        @Override
        public Piece getPiece() {
            return this.pieceOnSpecificTile;
        }
    }

}
