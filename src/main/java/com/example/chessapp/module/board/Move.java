package com.example.chessapp.module.board;

import com.example.chessapp.module.pieces.Piece;
import org.springframework.stereotype.Component;

@Component
public abstract class Move {

    // fields ------------------------------------------------------------------------------
    final Board board;
    final Piece movedPiece;
    final int destination;

    public Move(final Board board,final Piece movedPiece,final int destination) {
        this.board = board;
        this.movedPiece = movedPiece;
        this.destination = destination;
    }

    public static final class MajorMove extends Move{
        public MajorMove(final Board board, final Piece movedPiece, final int destination) {
            super(board, movedPiece, destination);
        }
    }

    public static final class Attack extends Move{
        final Piece underAttackPiece;
        public Attack(final Board board, final Piece movedPiece,
                      final int destination, final Piece underAttackPiece) {
            super(board, movedPiece, destination);
            this.underAttackPiece = underAttackPiece;
        }
    }

}
