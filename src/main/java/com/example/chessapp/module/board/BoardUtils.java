package com.example.chessapp.module.board;

import org.springframework.stereotype.Component;

public class BoardUtils {

    // fields ------------------------------------------------------------------------------------
    public static final boolean[] FIRST_COLUMN = initColumns(0);
    public static final boolean[] SECOND_COLUMN = initColumns(1);
    public static final boolean[] SEVENTH_COLUMN = initColumns(6);
    public static final boolean[] EIGHTH_COLUMN = initColumns(7);
    public static final int NUMBER_OF_TILES = 64;
    public static final int NUMBER_OF_TILES_PER_ROW = 8;


    // constructors -------------------------------------------------------------------------------
    // this is a utility class that can't be instantiated

    private BoardUtils() {
    }

    // methods -----------------------------------------------------------------------------------
    // static method that would be useful
    // to check the destination of a move,
    // to check if it is on the board or not
    public static boolean isTileWithinBound(int coordinate) {
        return coordinate >= 0 && coordinate < 64;
    }

    // for initializing columns based on the input
    public static boolean[] initColumns(final int columnNumber){
        final boolean[] column = new boolean[NUMBER_OF_TILES];
        for (int i = columnNumber; i < NUMBER_OF_TILES; i+=NUMBER_OF_TILES_PER_ROW) {
            System.out.println(i);
            column[i] = true;
        }
        return column;
    }

}
