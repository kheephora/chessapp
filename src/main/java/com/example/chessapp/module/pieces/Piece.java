package com.example.chessapp.module.pieces;

import com.example.chessapp.module.board.Board;
import com.example.chessapp.module.board.Move;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Getter
public abstract class Piece {


    // fields

    protected final Integer piecePosition;
    protected final Side pieceSide;

    public Piece(int piecePosition, Side pieceSide)  {
        this.piecePosition = piecePosition;
        this.pieceSide = pieceSide;
    }

    public abstract Collection<Move> calculateLegalMoves(final Board board);

}
