package com.example.chessapp.module.pieces;

import com.example.chessapp.module.board.Board;
import com.example.chessapp.module.board.BoardUtils;
import com.example.chessapp.module.board.Move;
import com.example.chessapp.module.board.Tile;
import com.google.common.collect.ImmutableList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Rook extends Piece{
    private final static int[] POSSIBLE_MOVES = {8, 1, -1, -8};

    public Rook(Integer piecePosition, Side pieceSide) {
        super(piecePosition, pieceSide);
    }

    @Override
    public Collection<Move> calculateLegalMoves(final Board board) {

        // destination coordinate for the move
        int possibleDestination;
        // list of legal moves
        final List<Move> legalMoves = new ArrayList<>();

        for (final int move : POSSIBLE_MOVES) {
            possibleDestination = this.piecePosition;

            while (BoardUtils.isTileWithinBound(possibleDestination)) {

                //if statement here for edge cases
//                if (isFirstColumn(this.piecePosition, move) ||
//                        isEighthColumn(this.piecePosition, move)) {
//                        break;
//                }

                possibleDestination += move;

                if (BoardUtils.isTileWithinBound(possibleDestination)) {
                    final Tile possibleDestinationTile = board.getTile(possibleDestination);
                    if (!possibleDestinationTile.isOccupied()) {
                        legalMoves.add(new Move.MajorMove(board, this, possibleDestination));
                    } else {
                        final Piece pieceAtTheDestination = possibleDestinationTile.getPiece();
                        final Side pieceSide = pieceAtTheDestination.getPieceSide();

                        if (this.pieceSide != pieceSide) {
                            legalMoves.add(new Move.Attack(board, this, possibleDestination, pieceAtTheDestination));
                        }
                        break;
                    }
                }
            }
        }

        return ImmutableList.copyOf(legalMoves);
    }

    // edge cases ------------------------------------------------------------------------------------
    /*
     * checking for an edge case, where the current position, starting position
     */

    private static void isFirstColumn(final int currentPos, final int possibleMove) {
        // edge case for bishop when on first column. -1 does not apply.
    }

    private static void isEigthColumn(final int currentPos, final int possibleMove) {
        // edge case for bishop when on last column. +1 does not apply.
    }

}
