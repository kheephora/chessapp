package com.example.chessapp.module.pieces;

import org.springframework.stereotype.Component;


public enum Side {
    BLACK,
    WHITE
}
