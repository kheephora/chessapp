package com.example.chessapp.module.pieces;

import com.example.chessapp.module.board.Board;
import com.example.chessapp.module.board.BoardUtils;
import com.example.chessapp.module.board.Move;
import com.example.chessapp.module.board.Tile;
import com.google.common.collect.ImmutableList;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Knight extends Piece {

    //fields -------------------------------------------------------------------------------------------
    private final static int[] POSSIBLE_MOVES = {-17, -15, -10, -6, 6, 10, 15, 17};

    // constructors -----------------------------------------------------------------------------------
    public Knight(Integer piecePosition, Side pieceSide) {
        super(piecePosition, pieceSide);
    }

    // methods -----------------------------------------------------------------------------------------

    // a method to find all possible legal moves
    @Override
    public Collection<Move> calculateLegalMoves(final Board board) {

        // destination coordinate for the move
        int possibleDestination;
        // list of legal moves
        final List<Move> legalMoves = new ArrayList<>();

        // loop through possible moves and find destination for each possible move
        for (int move : POSSIBLE_MOVES) {
            possibleDestination = this.piecePosition + move;

            if (BoardUtils.isTileWithinBound(possibleDestination)) {

                //if tile positions 0, 8, 16, 24, 32, 40, 48, 56
                // we are skipping rest of the code and going through next loop
                if (isFirstColumn(this.piecePosition, move) ||
                        isSecondColumn(this.piecePosition, move) ||
                        isSeventhColumn(this.piecePosition, move) ||
                        isEighthColumn(this.piecePosition, move)) {

                    continue;
                }

                final Tile possibleDestinationTile = board.getTile(possibleDestination);
                if (!possibleDestinationTile.isOccupied()) {
                    legalMoves.add(new Move.MajorMove(board, this, possibleDestination));
                } else {
                    final Piece pieceAtTheDestination = possibleDestinationTile.getPiece();
                    final Side pieceSide = pieceAtTheDestination.getPieceSide();

                    if (this.pieceSide != pieceSide) {
                        legalMoves.add(new Move.Attack(board, this, possibleDestination, pieceAtTheDestination));
                    }
                }
            }
        }
        return ImmutableList.copyOf(legalMoves);
    }

    // edge cases ------------------------------------------------------------------------------------
    /*
     * checking for an edge case, where the current position, starting position
     * of a move is on the first column(0, 8, 16, 24 etc.) and the offset is one of
     *  6, 15, -10, -17,. In this scenario destination is not one of legal moves
     * */
    private static boolean isFirstColumn(final int currentPos, final int possibleMove) {
        return BoardUtils.FIRST_COLUMN[currentPos] && ((possibleMove == 6) || (possibleMove == 15) ||
                (possibleMove == -10) || (possibleMove == -17));
    }

    /*
     * checking for an edge case, where the current position, starting position
     * of a move is on the second column(1, 9, 17, 25 etc.) and the offset is one of
     *  6, -10. In this scenario destination is not one of the legal moves
     * */
    private static boolean isSecondColumn(final int currentPos, final int possibleMove) {
        return BoardUtils.SECOND_COLUMN[currentPos] && ((possibleMove == 6) || (possibleMove == -10));
    }

    /*
     * checking for an edge case, where the current position, starting position
     * of a move is on the seventh column(6, 14, 22, 30 etc.) and the offset is one of
     *  10, -6. In this scenario destination is not one of the legal moves
     * */
    private static boolean isSeventhColumn(final int currentPos, final int possibleMove) {
        return BoardUtils.SEVENTH_COLUMN[currentPos] && ((possibleMove == 10) || (possibleMove == -6));
    }

    /*
     * checking for an edge case, where the current position, starting position
     * of a move is on the eighth column(7, 15, 23, 31 etc.) and the offset is one of
     *  10, 17, -6, -15. In this scenario destination is not one of the legal moves
     * */
    private static boolean isEighthColumn(final int currentPos, final int possibleMove) {
        return BoardUtils.EIGHTH_COLUMN[currentPos] && ((possibleMove == 10) || (possibleMove == 17) ||
                (possibleMove == -6) || (possibleMove == -15));
    }



}
