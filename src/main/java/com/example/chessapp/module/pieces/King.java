package com.example.chessapp.module.pieces;

import com.example.chessapp.module.board.Board;
import com.example.chessapp.module.board.Move;

import java.util.Collection;

public class King extends Piece{
    public King(int piecePosition, Side pieceSide) {
        super(piecePosition, pieceSide);
    }

    @Override
    public Collection<Move> calculateLegalMoves(Board board) {
        return null;
    }
}
